// je kan meer info vinden op paperjs.org Ik zou het graag snappen want 't is echt een fantastisch iets om mee te leren werken denk ik.
// de SVG is niet geimporteerd in het canvas waardoor het dus niks te maken heeft met "canvas"
// Adapted from the following Processing example: http://processing.org/learning/topics/follow3.html
// bovenstaande link is ook interessant trouwens. Als ik javascript OOIT vat, dan kan ik  mij eindelijk eens uitleven in Processing! :)
// Merci om de tijd te nemen Wouter!

/* Background setup */
// Create a symbol, which we will use to place instances of later:
var BLUE = new Color('#009BEF');
var RED = new Color('red');
var targetColor = BLUE;
var currentColor = new Color(BLUE);
var numberOfCircles = 50;
var pathbolletje = new Path.Circle({
  center: [0, 0],
  radius: 2,
  fillColor: currentColor,
});
var symbol = new Symbol(pathbolletje);

// Place the instances of the symbol:
for (var i = 0; i < numberOfCircles; i++) {
  // The center position is a random point in the view:
  var center = Point.random() * view.size;
  var placedSymbol = symbol.place(center);
}

/* Mouse trail setup */
// Number of points in the trail
var points = 20;
// The distance between the points:
var length = 35;
// The connect treshold
var connectTreshold = 120;
var connections = new Array(numberOfCircles);
for (var i = 0; i < numberOfCircles; i++) {
  connections[i] = new Array(points);
}

var pathOptions = {
  strokeColor: 'white',
  strokeWidth: 0
};
var path = new Path(pathOptions);

var start = view.center / [10, 1];
for (var i = 0; i < points; i++) path.add(start + new Point(i * length, 1));

/* Import Logo */
var logo;
project.importSVG('./img/logo.svg', {
  insert: false, 
  onLoad: function(svgLogo) {
    logo = svgLogo;
    logo.position = view.center;
    var newLayer = new Layer(logo);
    project.insertLayer(1, newLayer);
    logo.onClick = function(event) {
      console.log('opening link');
      window.open('http://grotezk.be');
    };
  }
});

// The onFrame function is called up to 60 times a second:
function onFrame(event) {
  if (!currentColor.equals(targetColor)) {
    currentColor = interpolateColor(currentColor, targetColor);
    pathbolletje.fillColor = currentColor;
  }
  // Run through the active layer's children list and change
  // the position of the placed symbols:
  for (var i = 0; i < numberOfCircles; i++) {
    var item = project.activeLayer.children[i];

    // Move the item 1/20th of its width to the right. This way
    // larger circles move faster than smaller circles:
    item.position.x += i / 25;

    // If the item has left the view on the right, move it back
    // to the left:
    if (item.position.x > view.size.width) {
      item.position.x = 0;
    }

    // Run through the mouse trail and connect if we are closer than connectTreshold
    for (var j = 0; j < points; j++) {
      var linePoint = path.segments[j].point;
      if (linePoint.getDistance(item.position) >= connectTreshold) {
        if (connections[i][j]) connections[i][j].remove();
        connections[i][j] = undefined;
        continue;
      }
      if (connections[i][j]) {
        connections[i][j].segments[0] = new Segment(item.position);
        connections[i][j].segments[1] = new Segment(linePoint);
        connections[i][j].strokeColor = currentColor;
      } else {
        connections[i][j] = new Path.Line(new Point(item.position), new Point(linePoint));
        connections[i][j].strokeColor = currentColor;
      }
    }
  }
}

// Update the mouse trail when we move the mouse
function onMouseMove(event) {
  path.firstSegment.point = event.point;
  for (var i = 0; i < points - 1; i++) {
      var segment = path.segments[i];
      var nextSegment = segment.next;
      var vector = segment.point - nextSegment.point;
      vector.length = length;
      nextSegment.point = segment.point - vector;
  }
  path.smooth();
  
  if (event.point.isInside(logo.bounds)) {
    targetColor = RED;
    document.body.style.cursor = 'pointer';
  } else {
    targetColor = BLUE;
    document.body.style.cursor = 'default';
  }
}

/**
 * Return the next color between two given colors
 * TODO: take the timestep into account
 * @param current {Color} The current color to use as a basis
 * @param target {Color} The color we want to animate to
 * #return {Color}
 */
function interpolateColor(current, target) {
  if (current.red > target.red) {
    current.red -= 0.01;
    if (current.red < target.red) current.red = target.red;
  } else {
    current.red += 0.01;
    if (current.red > target.red) current.red = target.red;
  }
  if (current.green > target.green) {
    current.green -= 0.01;
    if (current.green < target.green) current.green = target.green;
  } else {
    current.green += 0.01;
    if (current.green > target.green) current.green = target.green;
  }
  if (current.blue > target.blue) {
    current.blue -= 0.01;
    if (current.blue < target.blue) current.blue = target.blue;
  } else {
    current.blue += 0.01;
    if (current.blue > target.blue) current.blue = target.blue;
  }
  return current;
}
